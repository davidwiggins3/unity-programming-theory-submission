using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Duck : Animal  // INHERITANCE
{
    public override void Says()
    {
        sayLabel.text = "Quack";
    }
}
