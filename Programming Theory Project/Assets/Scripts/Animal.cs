using UnityEngine;
using TMPro;

public abstract class Animal : MonoBehaviour
{
    protected TextMeshProUGUI sayLabel { get; private set; } // ENCAPSULATION

    // Start is called before the first frame update
    void Start()
    {
        InitializeLocalVariables(); // ABSTRACTION
    }

    private void InitializeLocalVariables()
    {
        sayLabel = GameObject.Find("Animal Says Text").GetComponent<TextMeshProUGUI>();
    }

    public abstract void Says(); // POLYMORPHISM
    
    private void OnMouseDown() {
        Says();
    }
}
