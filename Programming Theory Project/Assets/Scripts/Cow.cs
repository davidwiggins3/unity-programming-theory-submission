using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cow : Animal   // INHERITANCE
{
    public override void Says()
    {
        sayLabel.text = "Moo";
    }
}
