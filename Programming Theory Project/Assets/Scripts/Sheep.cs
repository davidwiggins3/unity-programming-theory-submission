using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sheep : Animal // INHERITANCE
{
    public override void Says()
    {
        sayLabel.text = "Baa";
    }
}
