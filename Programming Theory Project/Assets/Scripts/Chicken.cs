using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chicken : Animal   // INHERITANCE
{
    public override void Says()
    {
        sayLabel.text = "Cluck";
    }
}
